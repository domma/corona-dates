from icalendar import Calendar, Event
from datetime import datetime
import os
import os.path


OUTPUT_FOLDER = "public"


cal = Calendar()

event = Event()
event["uid"] = "id-123"
event.add("summary", "some example event")
event.add("dtstart", datetime(2021,11,30,12,0,0))

cal.add_component(event)

if not os.path.exists(OUTPUT_FOLDER):
    os.mkdir(OUTPUT_FOLDER)

with open(f"{OUTPUT_FOLDER}/all.ical", "wb") as out:
    out.write(cal.to_ical())



with open(os.path.join(OUTPUT_FOLDER, "index.html"), "w") as out:
    out.write("hello corona dates")
